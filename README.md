# Code Challenge

Code Challenge for Airwallex Interview

## Preview

Project is deployed on [Heroku](https://code-challenge-host.herokuapp.com/).

## How to build on your local environment

Pre installation:

    nodejs
    yarn

Package Used:

    express (for deploy only)
    typescript
    jest
    react
    antd
    emotion
    axios

Install it and run on local development:

    yarn
    yarn dev (for dev)

Code quality:

    yarn lint-staged
    yarn test

## File Structure

```
📦.husky // For pre-commit script
┣ 📂\_
┃ ┣ 📜.gitignore
┃ ┗ 📜husky.sh
┗ 📜pre-commit
📦public
┣ 📜favicon.ico
┗ 📜index.html
📦src
┣ 📂component
┃ ┣ 📂App
┃ ┃ ┣ 📜index.test.tsx
┃ ┃ ┣ 📜index.tsx
┃ ┃ ┗ 📜styles.tsx
┃ ┗ 📂SignUpModal
┃ ┃ ┣ 📜index.test.tsx
┃ ┃ ┣ 📜index.tsx
┃ ┃ ┗ 📜styles.tsx
┣ 📜index.test.tsx
┗ 📜index.tsx
📜.babelrc
📜.eslintrc
📜.gitignore
📜.gitlab-ci.yml
📜.prettierrc
📜README.md
📜customTheme.ts // override antd styles
📜jest-setup.ts
📜jest.config.js
📜package.json
📜server.js // express server for deploy purpose
📜tsconfig.json
📜webpack.config.ts
📜yarn.lock
```
