export const antdOverrides = {
  'primary-color': '#eab863',
  'text-color': '#808080',
  'heading-color': '#808080',
  'font-family': 'Monaco',
};
