import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import App from '.';

afterEach(cleanup);

describe('Test by rendered UI', () => {
  it('renders with correct text', () => {
    render(<App />);
    const textList = [
      'BROCCOLI & CO.',
      'A better way',
      'to enjoy every day.',
      'Be the first to know when we launch',
      'Made with ♥ in Melbourne',
      '© 2016 Broccoli & Co. All rights reserved.',
    ];
    const elementList = textList.map((i) => screen.getByText(i));
    elementList.forEach((i) => expect(i).toBeInTheDocument());
  });

  it('renders modal when user click on request button', () => {
    const { getByTestId } = render(<App />);
    fireEvent.click(getByTestId('requestButton'));
    expect(getByTestId('SignUpModal')).toBeInTheDocument();
  });

  it('remove modal when user click outside of Modal', () => {
    const { queryByTestId } = render(<App />);
    fireEvent.click(screen.getByText('BROCCOLI & CO.'));
    expect(queryByTestId('SignUpModal')).not.toBeInTheDocument();
  });
});
