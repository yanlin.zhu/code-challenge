/** @jsx jsx */
import { jsx } from '@emotion/react';
import { FC, useState } from 'react';
import { hot } from 'react-hot-loader';
import {
  CenteredContent,
  FooterTextContainer,
  Heading,
  RootContainer,
  FixedFooter,
  FixedHeader,
  CenterTextContainer,
  BodyHeading,
  BodyText,
  BodyButton,
} from './styles';
import { SignUpModal } from '../SignUpModal';

const App: FC = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <RootContainer>
      <FixedHeader>
        <Heading>
          <b>BROCCOLI & CO.</b>
        </Heading>
        <hr />
      </FixedHeader>
      <CenteredContent>
        <CenterTextContainer>
          <BodyHeading>A better way</BodyHeading>
          <BodyHeading>to enjoy every day.</BodyHeading>
          <BodyText> Be the first to know when we launch</BodyText>
          <BodyButton data-testid={'requestButton'} size={'large'} onClick={() => setShowModal(true)}>
            Request an invite
          </BodyButton>
        </CenterTextContainer>
      </CenteredContent>

      <FixedFooter>
        <hr />
        <FooterTextContainer>
          <h4>Made with &hearts; in Melbourne</h4>
          <h4>&copy; 2016 Broccoli & Co. All rights reserved.</h4>
        </FooterTextContainer>
      </FixedFooter>
      <SignUpModal visibility={showModal} setShowModal={setShowModal} />
    </RootContainer>
  );
};

export default hot(module)(App);
