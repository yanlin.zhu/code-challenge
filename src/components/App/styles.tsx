import styled from '@emotion/styled';
import { Button } from 'antd';

export const RootContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

export const FixedFooter = styled.footer`
  width: 100%;
  text-align: center;
`;

export const FixedHeader = styled.header`
  width: 100%;
  font-family: Arial;
`;

export const CenteredContent = styled.main`
  display: table;
  flex: 1;
  overflow-y: auto;
`;

export const CenterTextContainer = styled.div`
  display: table-cell;
  text-align: center;
  vertical-align: middle;
`;

export const BodyHeading = styled.div`
  font-size: 4vmax;
  line-height: 1;
`;

export const BodyText = styled.div`
  font-size: 1.5vmax;
  line-height: 1;
  margin-top: 1.5vmax;
  font-color: #dcdcdc;
`;

export const BodyButton = styled(Button)`
  margin-top: 1vmax;
  border: 1px solid black;
`;

export const Heading = styled.h3`
  margin-top: 24px;
  margin-bottom: 24px;
  margin-left: 10%;
`;

export const FooterTextContainer = styled.div`
  margin-top: 18px;
  margin-bottom: 18px;
  h4 {
    font-style: italic;
  }
`;
