import { cleanup, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { SignUpModal } from '.';
import userEvent from '@testing-library/user-event';

jest.mock('axios');

afterEach(cleanup);

describe('Test by rendered UI', () => {
  it('renders with correct text for form modal', () => {
    render(<SignUpModal visibility={true} setShowModal={jest.fn} />);
    const textList = ['Request an invite', 'Send'];
    const elementList = textList.map((i) => screen.getByText(i));
    elementList.forEach((i) => expect(i).toBeInTheDocument());
  });
});

describe('Test Form Function', () => {
  it('user Click send without entering information', async () => {
    render(<SignUpModal visibility={true} setShowModal={jest.fn} />);

    userEvent.click(screen.getByText('Send'));

    await waitFor(() => {
      expect(screen.getByText('Please input your full name!'));
      expect(screen.getByText('Please input your E-mail!'));
      expect(screen.getByText('Please confirm your email!'));
    });
  });

  it('form enter empty spaces for name field', async () => {
    const { getByTestId } = render(<SignUpModal visibility={true} setShowModal={jest.fn} />);

    userEvent.type(getByTestId('name'), '     ');

    await waitFor(() => expect(screen.getByText('Your full name can not be empty spaces!')));
  });

  it('user enters invalid email', async () => {
    const { getByTestId } = render(<SignUpModal visibility={true} setShowModal={jest.fn} />);

    userEvent.type(getByTestId('email'), '123');

    await waitFor(() => expect(screen.getByText('The input is not valid E-mail!')));
  });

  it('user enters different email in confirmation', async () => {
    const { getByTestId } = render(<SignUpModal visibility={true} setShowModal={jest.fn} />);

    userEvent.type(getByTestId('email'), '123@qq.com');
    userEvent.type(getByTestId('confirm'), '1234@qq.com');

    await waitFor(() => expect(screen.getByText('The two emails that you entered do not match!')));
  });
});
