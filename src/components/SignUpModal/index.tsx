/** @jsx jsx */
import { jsx } from '@emotion/react';
import { Modal, Form, Input, notification } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import axios from 'axios';
import { FC, useState } from 'react';
import {
  ErrorDisplay,
  ModalBody,
  ModalBodyText,
  ModalButton,
  ModalContainer,
  ModalDivider,
  ModalHeading,
} from './styles';

const backendUrl = 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth';

type IProps = {
  visibility: boolean;
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
};

export const SignUpModal: FC<IProps> = ({ visibility, setShowModal }) => {
  const [isCompleted, setIsCompleted] = useState<boolean>(false);
  const [isSending, setIsSending] = useState<boolean>(false);
  const [responseError, setResponseError] = useState<string>('');

  const [form] = useForm();

  const onClose = () => {
    setIsCompleted(false);
    form.resetFields();
    setShowModal(false);
  };

  const handleSubmit = () => {
    setIsSending(true);
    const data = {
      // remove potential empty spaces
      name: form.getFieldValue('name').trim(),
      email: form.getFieldValue('email'),
    };
    axios
      .post(backendUrl, data)
      .then((res) => {
        setIsSending(false);
        setIsCompleted(true);
        setResponseError('');
        notification.success({ message: 'SUCCESS', description: res.data });
      })
      .catch((error) => {
        // handle response error
        if (error.response) {
          setResponseError(error.response.data.errorMessage);
          notification.error({ message: 'FAILURE', description: error.response.data.errorMessage });
        }
        // handle no response error
        else if (error.request) {
          setResponseError(error.request);
          notification.error({ message: 'FAILURE', description: error.request });
        }
        // handle request error
        else {
          setResponseError(error.message);
          notification.error({ message: 'FAILURE', description: error.message });
        }
        setIsSending(false);
      });
  };

  return (
    <Modal data-testid={'SignUpModal'} centered visible={visibility} closable={false} onCancel={onClose} footer={null}>
      <ModalContainer>
        <ModalHeading>{isCompleted ? 'All done!' : 'Request an invite'}</ModalHeading>
        <ModalDivider />
        {isCompleted ? (
          <ModalBody>
            <ModalBodyText>You will be one of the first to experience</ModalBodyText>
            <ModalBodyText>Broccoli & Co. when we launch.</ModalBodyText>
          </ModalBody>
        ) : (
          <ModalBody>
            <Form id="SignUpForm" form={form} onFinish={handleSubmit}>
              <Form.Item
                name="name"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please input your full name!',
                  },
                  () => ({
                    validator(_, value) {
                      if (!value || !/^ *$/.test(value)) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Your full name can not be empty spaces!'));
                    },
                  }),
                ]}
              >
                <Input data-testid={'name'} placeholder={'Full name'} />
              </Form.Item>
              <Form.Item
                name="email"
                hasFeedback
                rules={[
                  {
                    type: 'email',
                    message: 'The input is not valid E-mail!',
                  },
                  {
                    required: true,
                    message: 'Please input your E-mail!',
                  },
                ]}
              >
                <Input data-testid={'email'} placeholder={'Email'} />
              </Form.Item>
              <Form.Item
                name="confirm"
                dependencies={['email']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please confirm your email!',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('email') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('The two emails that you entered do not match!'));
                    },
                  }),
                ]}
              >
                <Input data-testid={'confirm'} placeholder={'Confirm Email'} />
              </Form.Item>
            </Form>
          </ModalBody>
        )}
        {isCompleted ? (
          <ModalButton onClick={onClose}>OK</ModalButton>
        ) : (
          <ModalButton htmlType="submit" onClick={() => form.submit()} loading={isSending} disabled={isSending}>
            {isSending ? 'Sending, please wait...' : 'Send'}
          </ModalButton>
        )}
        <ErrorDisplay>{responseError}</ErrorDisplay>
      </ModalContainer>
    </Modal>
  );
};
