import styled from '@emotion/styled';
import { Button } from 'antd';

export const ModalContainer = styled.div`
  margin-top: 10%;
  margin-bottom: 10%;
  text-align: center;
`;

export const ModalHeading = styled.h2`
  font-style: italic;
`;

export const ModalDivider = styled.hr`
  border: 1px solid;
  width: 10%;
`;

export const ModalBody = styled.div`
  margin-top: 10%;
  margin-bottom: 10%;
`;

export const ModalBodyText = styled.div`
  font-weight: lighter;
  font-color: #dcdcdc;
`;

export const ModalButton = styled(Button)`
  width: 80%;
  border: 1px solid black;
`;

export const ErrorDisplay = styled.div`
  margin-top: 5%;
  font-style: italic;
  color: red;
`;
